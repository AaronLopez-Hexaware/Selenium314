package claseUno;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class DemoQA {

	public static void main(String[] args) {
	
		WebDriver driver;
		WebElement element;
		Select s;
		
		
		String driverPath = "C://Selenium//chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", driverPath);
	
		driver = new ChromeDriver();
	
		driver.manage().window().maximize();
		
		
		try {
			
			driver.navigate().to("https://www.demoqa.com/registration");
			
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}
		
		// id name_3_firstname => Aaron
		element = driver.findElement(By.id("name_3_firstname"));
		element.sendKeys("Aaron");
		
		// id name_3_lastname => Lopez
		element = driver.findElement(By.id("name_3_lastname"));
		element.sendKeys("Lopez");
		
		// xpath //*[@value="single"] => Single
		element = driver.findElement(By.xpath("//*[@value=\"single\"]"));
		element.click();
		
		// xpath (//*[@name="checkbox_5[]"])[2] => reading
		element = driver.findElement(By.xpath("(//*[@name=\"checkbox_5[]\"])[2]"));
		element.click();
		
		
		// Select Country id=> dropdown_7 Mexico
		element = driver.findElement(By.id("dropdown_7"));
		s = new Select(element);
		s.selectByVisibleText("Mexico");
		
		// id mm_date_8 month => 7
		
		element = driver.findElement(By.id("mm_date_8"));
		s = new Select(element);
		s.selectByIndex(7);
		
		// id dd_date_8 day=> 8
		element = driver.findElement(By.id("dd_date_8"));
		s = new Select(element);
		s.selectByVisibleText("8");
		
		// id yy_date_8 year => 1994
		element = driver.findElement(By.id("yy_date_8"));
		s = new Select(element);
		s.selectByValue("1994");;
		
		// phone_9 id 844 321 3454
		element = driver.findElement(By.id("phone_9"));
		element.sendKeys("8443213454");
		
		
		// name username aaronlopez24
		element = driver.findElement(By.name("username"));
		element.sendKeys("aaronlopez24");
		
		
		// name => e_mail aaronlopez24@hotmail.com
		element = driver.findElement(By.name("e_mail"));
		element.sendKeys("aaronlopez24@hotmail.com");
		
		
		// id => password_2
		element = driver.findElement(By.id("password_2"));
		element.sendKeys("a.lopezoliden.0807");
		
		// id => confirm_password_password_2 
		element = driver.findElement(By.id("confirm_password_password_2"));
		element.sendKeys("a.lopezoliden.0807");
		
		
		// strong pass id = "piereg_passwordStrength" 
		element = driver.findElement(By.id("piereg_passwordStrength"));
		if(element.getText().equals("Strong")) {
			
			System.out.println("Contrase;a segura");
			
			element = driver.findElement(By.name("pie_submit"));
			element.click();
		}
	}
}
