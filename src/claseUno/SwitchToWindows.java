package claseUno;

import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SwitchToWindows {

	static WebDriver driver;
	
	public static void main(String[] args) {


		
		WebElement element;
		WebDriverWait varWait;
		
		String driverPath = "C://Selenium//chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", driverPath);

		driver = new ChromeDriver();
		
		
		driver.manage().window().maximize();
		varWait = new WebDriverWait(driver, 10);

		// Navigate
		try {

			driver.navigate().to("http://demoqa.com/frames-and-windows/");

		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}

		
		element = driver.findElement(By.id("ui-id-2"));
		varWait.until(ExpectedConditions.elementToBeClickable(element)).click();
		
		element = driver.findElement(By.xpath("//*[text()=\"Open New Seprate Window\"]"));
		varWait.until(ExpectedConditions.visibilityOf(element)).click();
	
		SwitchToWindows();
		
		element = driver.findElement(By.className("dt-mobile-menu-icon"));
		varWait.until(ExpectedConditions.elementToBeClickable(element)).click();
		
	}
	
	public static void SwitchToWindows() {
		
		String currWin = driver.getWindowHandle();
		Set<String> windows = driver.getWindowHandles();
		windows.remove(currWin);
		
		for(String x : windows) {
			
			if(x != currWin)
				driver.switchTo().window(x);
			
		}
		
		
	}
	
}
